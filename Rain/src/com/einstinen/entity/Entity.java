package com.einstinen.entity;

import java.util.Random;

import com.einstinen.graphics.Screen;
import com.einstinen.level.Level;

public class Entity
{
	protected int x, y, z;
	private boolean removed = false;
	protected Level level;
	protected final Random random = new Random();

	public void update()
	{

	}

	public void render(Screen screen)
	{

	}

	public void remove()
	{
		// removed from level
		removed = true;
	}

	public boolean isRemoved()
	{
		return removed;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public int getZ()
	{
		return z;
	}

}
