package com.einstinen.entity.mob;

import java.util.ArrayList;

import com.einstinen.entity.Entity;
import com.einstinen.graphics.Screen;
import com.einstinen.graphics.Sprite;
import com.einstinen.myMath.Vector3;

public abstract class Mob extends Entity
{
	protected float speed;
	protected Sprite sprite;
	protected int dir = 3;
	protected boolean moving = false;
	protected int xa = 0, ya = 0;
	public static ArrayList<Mob> Mobs = new ArrayList<Mob>();
	public static int index;

	public void move(int dx, int dy)
	{
		if(collision(dx, x, (dy), y) && !collision(0, x, (dy), y))
			y += dy;
		if(collision(dx, x, (dy), y) && !collision(dx, x, (0), y))
			x += dx;
		if (!collision(dx, x, (dy), y))
		{
			x += dx;
			y += dy;
		}
		if (dy < 0)
			dir = 0;
		else
			if (dy > 0 && dx == 0)
				dir = 2;
		if (dx > 0)
			dir = 1;
		else
			if (dx < 0)
				dir = 3;
		xa = ya = 0;
	}

	public void update()
	{

	}

	private boolean collision(int dx, int x, int dy, int y)
	{
		if(!level.getTile((x) / 16, (y) / 16, z).solid()) {
			if (level.getTile((dx + x) / 16, (dy + y) / 16, z).solid())
			{
				return true;
			}
			if (level.getTile((dx + x + 15) / 16, (dy + y) / 16, z).solid())
			{
				return true;
			}
			if (level.getTile((dx + x) / 16, (dy + y + 15) / 16, z).solid())
			{
				return true;
			}
			if (level.getTile((dx + x + 15) / 16, (dy + y + 15) / 16, z).solid())
			{
				return true;
			}
		}
		return false;
	}

	public void render(Screen screen)
	{

	}
}
