package com.einstinen.entity.mob;

import com.einstinen.Game;
import com.einstinen.graphics.Screen;
import com.einstinen.graphics.Sprite;
import com.einstinen.input.keyboard;
import com.einstinen.level.Level;

public class Player extends Mob
{
	private keyboard keys;
	private Sprite[] sprites = new Sprite[4];

	public Player(keyboard keys, Level level, int z)
	{
		this.keys = keys;
		this.level = level;
		this.z = z;
		sprites[0] = Sprite.player0;
		speed = 1;
		Mobs.add(this);
		index++;
	}

	public Player(int x, int y, int z, keyboard keys, Level level)
	{
		this.keys = keys;
		this.x = x;
		this.y = y;
		this.z = z;
		this.level = level;
		speed = 1;
		Mobs.add(this);
		index++;
	}

	public void update()
	{
		System.out.println(level.getTile(x / 16, y / 16, z).getI());
		if (keys.left)
			xa-=speed;
		if (keys.right)
			xa+=speed;
		if (keys.up)
			ya-=speed;
		if (keys.down)
			ya+=speed;
		if (xa != 0 || ya != 0)
			move(xa, ya);
		if (keys.com)
		{
			if (level.getTile((x + 8) >> 4, (y + 8) >> 4, z).down())
			{
				down();
			}
			if (level.getTile(x >> 4, y >> 4, z).down())
			{
				down();
			}
		}
		if (keys.per)
		{
			if (level.getTile((x + 8) >> 4, (y + 8) >> 4, z).up())
			{
				up();
			}
			if (level.getTile(x >> 4, y >> 4, z).up())
			{
				up();
			}
		}
		Game.worldLevel = z;

	}

	public void render(Screen screen)
	{
		if (dir == 0 || dir == 3)
			screen.renderMob(x, y, z, false, false, sprites[0]);
		else
			screen.renderMob(x, y, z, true, false, sprites[0]);
	}

	public void up()
	{
		System.out.println("up");
		z++;
	}

	public void down()
	{
		System.out.println("down");
		z--;
	}
}
