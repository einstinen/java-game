package com.einstinen.level.tile;

import com.einstinen.graphics.Screen;
import com.einstinen.graphics.Sprite;

public class ChickenTile extends Tile {
	
	public ChickenTile()
	{
		sprite = Sprite.chicken;
	}
}
