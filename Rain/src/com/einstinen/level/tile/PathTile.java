package com.einstinen.level.tile;

import com.einstinen.graphics.Screen;
import com.einstinen.graphics.Sprite;

public class PathTile extends Tile
{
	/**
	 * the path constructor.
	 * @param pathnum the number that identifies the paths per floor.
	 * @param i the number that identifies the tiles per path
	 */
	public PathTile(int pathnum, int segment)
	{
		super(pathnum, segment);
		sprite = Sprite.path;
		isPath = true;
	}
}
