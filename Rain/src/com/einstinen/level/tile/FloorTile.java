package com.einstinen.level.tile;

import com.einstinen.graphics.Screen;
import com.einstinen.graphics.Sprite;

public class FloorTile extends Tile {
	public FloorTile()
	{
		inRoom = true;
		sprite = Sprite.floor;		
	}
}
