package com.einstinen.level.tile;

import com.einstinen.graphics.Screen;
import com.einstinen.graphics.Sprite;

public class StoneTile extends Tile {
	public StoneTile()
	{
		sprite = Sprite.stone;
		isSolid = true;
	}
}
