package com.einstinen.level.tile;

import com.einstinen.graphics.Screen;
import com.einstinen.graphics.Sprite;

public class GrassTile extends Tile
{
	
	public GrassTile()
	{
		sprite = Sprite.grass;
	}
}
