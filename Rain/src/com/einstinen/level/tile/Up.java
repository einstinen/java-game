package com.einstinen.level.tile;

import com.einstinen.graphics.Screen;
import com.einstinen.graphics.Sprite;

public class Up extends Tile
{
	
	public Up()
	{
		sprite = Sprite.up;
		isUp = true;
	}

}
