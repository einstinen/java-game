package com.einstinen.level.tile;

import com.einstinen.graphics.Screen;
import com.einstinen.graphics.Sprite;

public class Down extends Tile
{
	
	public Down()
	{
		sprite = Sprite.down;
	}
}
