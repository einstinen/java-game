package com.einstinen.level.tile;

import com.einstinen.graphics.Screen;
import com.einstinen.graphics.Sprite;

//abstract
public class Tile
{
	/**
	 * i should be -1 for any tile out of a room or not a path, it represents what set of tiles or what room it belongs to.
	 */
	private int i = -1;
	/**
	 * is -1 for any tile not a path, represents how far along the path something is. 
	 */
	private int segment = -1;
	
	protected int x, y, oreType;
	public Sprite sprite;
	protected Sprite sprite2;
	protected Sprite sprite3;
	protected boolean isSolid = false, isUp = false, isDown = false, isLava = false, isWater = false, inRoom = false, isPath = false;

	public static Tile voidTile = new voidTile();
	protected Tile lower = voidTile;
	// check tile number every time a new tile is added

	public Tile()
	{
	}
	public Tile(int i)
	{
		setI(i);
	}
	public Tile(int i, int segment)
	{
		setI(i);
		setSegment(segment);
	}

	public void update()
	{
		
	}

	public void render(int x, int y, int z, Screen screen)
	{
		screen.renderTile(x << 4,  y << 4, z, this);
	}

	public boolean solid()
	{
		return isSolid;
	}

	public boolean up()
	{
		return isUp;
	}

	public boolean down()
	{
		return isDown;
	}

	public boolean water()
	{
		return isWater;
	}

	public boolean lava()
	{
		return isLava;
	}
	public boolean room()
	{
		return inRoom;
	}
	public boolean path()
	{
		return isPath;
	}
	public int ore()
	{
		return oreType;
	}
	public Tile dug()
	{
		return this;
	}
	public void setLower(Tile lower)
	{
		this.lower = lower;
	}
	public int getI()
	{
		return i;
	}
	public void setI(int i)
	{
		this.i = i;
	}
	public void setSegment(int segment)
	{
		this.segment = segment;
	}
	public int getSegment()
	{
		return segment;
	}
}
