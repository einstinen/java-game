package com.einstinen.level;

import java.util.ArrayList;

import com.einstinen.Game;
import com.einstinen.graphics.Screen;
import com.einstinen.level.tile.Tile;
import com.einstinen.myMath.Vector3;

public class Level
{

	protected int width, height, depth, pz;
	protected Tile[][] tiles;
	
	public Level(int width, int height, int depth)
	{
		this.width = width;
		this.height = height;
		this.depth = depth;
		pz = depth;
		tiles = new Tile[width * height][depth];
		generateLevel();
	}

	public Level(String path)
	{
		loadLevel(path);
	}

	protected void generateLevel()
	{

	}

	protected void loadLevel(String path)
	{

	}

	protected void generatedStructure(String path)
	{

	}

	public void update()
	{

	}

	protected void time()
	{

	}

	public void render(int xScroll, int yScroll, Screen screen)
	{
		int z = Game.worldLevel;
		screen.setOffset(xScroll, yScroll);
		int x0 = (xScroll) >> 4, y0 = (yScroll) >> 4;
		int x1 = (xScroll + screen.width + 16) >> 4, y1 = (yScroll
			+ screen.height + 16) >> 4;
		for (int y = y0; y < y1; y++)
		{
			for (int x = x0; x < x1; x++)
			{
				getTile(x, y, z).render(x, y, z, screen);
			}
		}
	}

	public Tile getTile(int x, int y, int z)
	{
		if(x < 0 || x >= width || y < 0 || y >= width)
			return Tile.voidTile;
		return tiles[x + y * width][z];
	}
}
