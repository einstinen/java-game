package com.einstinen.level;

import java.util.ArrayList;
import java.util.Random;

//import com.einstinen.entity.mob.Crawler;
import com.einstinen.level.tile.Down;
import com.einstinen.level.tile.FloorTile;
import com.einstinen.level.tile.GrassTile;
import com.einstinen.level.tile.PathTile;
import com.einstinen.level.tile.StoneTile;
import com.einstinen.level.tile.Tile;
import com.einstinen.level.tile.Up;
import com.einstinen.level.tile.WallTile;
import com.einstinen.myMath.Vector3;

public class RandomLevel extends Level
{
	private static final Random random = new Random();
	
	public RandomLevel(int width, int height, int depth)
	{
		super(width, height, depth);
	}
	//could probably break into submethods
	protected void generateLevel()
	{
		Vector3[] roomSp = new Vector3[depth];
		Vector3[] roomSpS = new Vector3[depth];
		Vector3[] roomSp2 = new Vector3[depth];
		Vector3[] roomSp2S = new Vector3[depth];
		Vector3[] down = new Vector3[depth];
		Vector3[] up = new Vector3[depth];
		int[][] roomCX = new int[width / 8][depth];
		int[][] roomCY = new int[roomCX.length][depth];
		int[][] roomX = new int[roomCX.length][depth];
		int[][] roomY = new int[roomCX.length][depth];
		Vector3[][] roomPathStart = new Vector3[roomCX.length + 2][depth];
		for (int z = 0; z < depth; z++)
		{
			if (z != 0)
			{
				roomSp[z] = new Vector3(random.nextInt(width - 9), random.nextInt(height - 7), z);
				roomSp2[z - 1] = new Vector3(roomSp[z].getX(), roomSp[z].getY(), z - 1);

				roomSpS[z] = new Vector3(3 + random.nextInt(5), 2 + random.nextInt(4), z);
				roomSp2S[z - 1] = new Vector3(roomSpS[z].getX(), roomSpS[z].getY(), z - 1);
			}
			// there shouldnt be a down or up for for the top and bottom floor respectively
		}
		for (int z = 0; z < depth; z++)
		{
			if (z != 0)
			{
				down[z] = new Vector3(1 + roomSp[z].getX() + random.nextInt(roomSpS[z].getX()),
						1 + roomSp[z].getY() + random.nextInt(roomSpS[z].getY()), z);
				roomPathStart[0][z] = new Vector3(1 + roomSp[z].getX() + random.nextInt(roomSpS[z].getX()),
						1 + roomSp[z].getY() + random.nextInt(roomSpS[z].getY()), z);

				up[z - 1] = new Vector3(down[z].getX(), down[z].getY(), z - 1);
				roomPathStart[roomCY.length + 1][z - 1] = new Vector3(
						1 + roomSp2[z - 1].getX() + random.nextInt(roomSp2S[z - 1].getX()),
						1 + roomSp2[z - 1].getY() + random.nextInt(roomSp2S[z - 1].getY()), z);
			}
			for (int i = 0; i < roomCX.length; i++)
			{
				roomCX[i][z] = random.nextInt((width - 9));
			}
			for (int i = 0; i < roomCY.length; i++)
			{
				roomCY[i][z] = random.nextInt((height - 7));
			}
			for (int i = 0; i < roomCX.length; i++)
			{
				roomX[i][z] = 3 + random.nextInt(5);
			}
			for (int i = 0; i < roomCY.length; i++)
			{
				roomY[i][z] = 2 + random.nextInt(4);
				roomPathStart[i + 1][z] = new Vector3(roomCX[i][z] + 1 + random.nextInt(roomX[i][z]),
						roomCY[i][z] + 1 + random.nextInt(roomY[i][z]), z);
			}
		}
		for (int z = 0; z < depth - 1; z++)
		{
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					tiles[x + y * width][z] = new StoneTile();
				}
			}
		}
		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++)
				tiles[x + y * width][depth - 1] = new GrassTile();
		for (int z = 0; z < depth; z++)
		{
			for (int i = 0; i < roomCX.length; i++)
			{
				tiles[roomCX[i][z] + roomCY[i][z] * width][z] = new WallTile();
				tiles[roomCX[i][z] + roomX[i][z] + 1 + roomCY[i][z] * width][z] = new WallTile();
				tiles[roomCX[i][z] + (roomY[i][z] + 1 + roomCY[i][z]) * width][z] = new WallTile();
				tiles[roomCX[i][z] + roomX[i][z] + 1 + (roomY[i][z] + 1 + roomCY[i][z]) * width][z] = new WallTile();

				for (int x = 0; x < roomX[i][z]; x++)
				{
					tiles[roomCX[i][z] + x + 1 + roomCY[i][z] * width][z] = new WallTile();
					tiles[roomCX[i][z] + x + 1 + (roomCY[i][z] + roomY[i][z] + 1) * width][z] = new WallTile();
				}
				for (int y = 0; y < roomY[i][z]; y++)
				{
					tiles[roomCX[i][z] + (roomCY[i][z] + y + 1) * width][z] = new WallTile();
					tiles[roomCX[i][z] + roomX[i][z] + 1 + (roomCY[i][z] + y + 1) * width][z] = new WallTile();
				}
			}
		}
		for (int z = 1; z < depth; z++)
		{
			tiles[roomSp[z].getX() + roomSp[z].getY() * width][z] = new WallTile();
			tiles[roomSp[z].getX() + 1 + roomSpS[z].getX() + roomSp[z].getY() * width][z] = new WallTile();
			tiles[roomSp[z].getX() + (roomSp[z].getY() + 1 + roomSpS[z].getY()) * width][z] = new WallTile();
			tiles[roomSp[z].getX() + 1 + roomSpS[z].getX()
					+ (roomSp[z].getY() + 1 + roomSpS[z].getY()) * width][z] = new WallTile();
			for (int x = 0; x < roomSpS[z].getX(); x++)
			{
				tiles[roomSp[z].getX() + 1 + x + roomSp[z].getY() * width][z] = new WallTile();
				tiles[roomSp[z].getX() + 1 + x + (roomSp[z].getY() + 1 + roomSpS[z].getY()) * width][z] = new WallTile();
			}
			for (int y = 0; y < roomSpS[z].getY(); y++)
			{
				tiles[roomSp[z].getX() + (y + 1 + roomSp[z].getY()) * width][z] = new WallTile();
				tiles[roomSp[z].getX() + 1 + roomSpS[z].getX() + (y + 1 + roomSp[z].getY()) * width][z] = new WallTile();
			}
			for (int x = 0; x < roomSpS[z].getX(); x++)
			{
				for (int y = 0; y < roomSpS[z].getY(); y++)
				{
					tiles[roomSp[z].getX() + x + 1 + (roomSp[z].getY() + y + 1) * width][z] = new FloorTile();
				}
			}
		}
		for (int z = 0; z < depth - 1; z++)
		{
			tiles[roomSp2[z].getX() + roomSp2[z].getY() * width][z] = new WallTile();
			tiles[roomSp2[z].getX() + 1 + roomSp2S[z].getX() + roomSp2[z].getY() * width][z] = new WallTile();
			tiles[roomSp2[z].getX() + (roomSp2[z].getY() + 1 + roomSp2S[z].getY()) * width][z] = new WallTile();
			tiles[roomSp2[z].getX() + 1 + roomSp2S[z].getX()
					+ (roomSp2[z].getY() + 1 + roomSp2S[z].getY()) * width][z] = new WallTile();
			for (int x = 0; x < roomSp2S[z].getX(); x++)
			{
				tiles[roomSp2[z].getX() + 1 + x + roomSp2[z].getY() * width][z] = new WallTile();
				tiles[roomSp2[z].getX() + 1 + x + (roomSp2[z].getY() + 1 + roomSp2S[z].getY()) * width][z] = new WallTile();
			}
			for (int y = 0; y < roomSp2S[z].getY(); y++)
			{
				tiles[roomSp2[z].getX() + (y + 1 + roomSp2[z].getY()) * width][z] = new WallTile();
				tiles[roomSp2[z].getX() + 1 + roomSp2S[z].getX() + (y + 1 + roomSp2[z].getY()) * width][z] = new WallTile();
			}
			for (int x = 0; x < roomSp2S[z].getX(); x++)
			{
				for (int y = 0; y < roomSp2S[z].getY(); y++)
				{
					tiles[roomSp2[z].getX() + x + 1 + (roomSp2[z].getY() + y + 1) * width][z] = new WallTile();
				}
			}
		}
		for (int z = 0; z < depth; z++)
		{
			for (int i = 0; i < roomCX.length; i++)
			{

				for (int x = 0; x < roomX[i][z]; x++)
				{
					for (int y = 0; y < roomY[i][z]; y++)
					{
						tiles[roomCX[i][z] + x + 1 + (roomCY[i][z] + y + 1) * width][z] = new FloorTile();
					}
				}
			}
		}
		for (int z = 0; z < depth; z++)
		{
			int segment = 0;
			for (int i = 0; i < roomCX.length + 1; i++)
			{
				if (!(i > roomCX.length))
				{
					if (!(z == 0 && i == 0))
					{
						Vector3 current = new Vector3(roomPathStart[i][z].getX(), roomPathStart[i][z].getY(),z);
//						new Crawler(current.getX() << 4,current.getY() << 4,z,this);
						boolean pathing = true;
						while (pathing)
						{
							try
							{
								roomPathStart[i + 1][z].toString();
							}
							catch (ArrayIndexOutOfBoundsException e)
							{
								System.out.println("caughtNull");
								pathing = false;
								continue;
							}
							catch (NullPointerException e)
							{
								System.out.println("caughtNull");
								pathing = false;
								continue;
							}
							if (current.getX() < roomPathStart[i + 1][z].getX())
							{
								current = new Vector3(current.addX(1));
								if(tiles[current.getX() + current.getY() * width][z].room())
								{
									tiles[current.getX() + current.getY() * width][z] = new FloorTile();
								}
								else
								{
									tiles[current.getX() + current.getY() * width][z] = new PathTile(i, segment++);
								}
							}
							if (current.getX() > roomPathStart[i + 1][z].getX())
							{
								current = new Vector3(current.addX(-1));
								if(tiles[current.getX() + current.getY() * width][z].room())
								{
									tiles[current.getX() + current.getY() * width][z] = new FloorTile();
								}
								else
								{
									tiles[current.getX() + current.getY() * width][z] = new PathTile(i, segment++);
								}
							}
							if (current.getY() < roomPathStart[i + 1][z].getY())
							{
								current = new Vector3(current.addY(1));
//								PathTileIndex.addPath(current, i , z);
								if(tiles[current.getX() + current.getY() * width][z].room())
								{
									tiles[current.getX() + current.getY() * width][z] = new FloorTile();
								}
								else
								{
									tiles[current.getX() + current.getY() * width][z] = new PathTile(i, segment++);
								}
							}
							if (current.getY() > roomPathStart[i + 1][z].getY())
							{
								current = new Vector3(current.addY(-1));
								if(tiles[current.getX() + current.getY() * width][z].room())
								{
									tiles[current.getX() + current.getY() * width][z] = new FloorTile();
								}
								else
								{
									tiles[current.getX() + current.getY() * width][z] = new PathTile(i,segment++);
								}
							}
							if(current.equals(roomPathStart[i + 1][z]))
							{
								pathing = false;
							}
						}
					}
				}
			}
		}
		for (int z = 0; z < depth; z++)
		{
			if (z != 0)
			{
				tiles[down[z].getX() + down[z].getY() * width][z] = new Down();
			}
			if (z != depth - 1)
			{
				tiles[up[z].getX() + up[z].getY() * width][z] = new Up();
			}
		}
		inRoom();
	}
	/**
	 * sets the Vector3 array rooms. 
	 * should be called immediately after generateLevel 
	 */
	private void inRoom()
	{
		//each room has an index
		boolean[][][] room = new boolean[width][height][depth];
		int[][][] index = new int [width][height][depth];
		for (int z = 0; z < this.depth; z ++)
		{
			for (int y = 0; y < this.height; y ++)
			{
				for (int x = 0; x < this.width; x ++)
				{
					if(getTile(x, y, z).room())
					{
						room[x][y][z] = true;
					}
				}
			}
			int i = 0;
			int I = 1;
			boolean secondRow = false;
			for (int y = 0; y < this.height; y ++)
			{
				boolean secondCol = false;
				for (int x = 0; x < this.width; x ++)
				{
					index[x][y][z] = -1;
					if (secondRow && room[x][y][z] && room[x][y - 1][z])
					{
						i = index[x][y - 1][z];
					}
					if (room[x][y][z])
					{
						index[x][y][z] = i;
						I = i + 1;
					}
					else if (secondCol && room[x - 1][y][z])
					{
							i = I;							
					}
					secondCol = true;
				}
				secondRow = true;
			}
			for (int y = 0; y < this.height; y ++)
			{
				for (int x = 0; x < this.width; x ++)
				{
					if (tiles[x + y * width][z].room())
					{
						tiles[x + y * width][z].setI(index[x][y][z]);
					}
				}
			}
		}
	}
}
