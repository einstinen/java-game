package com.einstinen.myMath;

public class Vector3
{
	public static final Vector3 ZERO = new Vector3(0,0,0);
	private int x, y, z;
	private boolean isNull = true;
	public Vector3(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		isNull = false;
	}

	public Vector3(Vector3 current)
	{
		x = current.getX();
		y = current.getY();
		z = current.getZ();
	}

	public Vector3 addVectors(Vector3 a1, Vector3 a2)
	{
		Vector3 a3 = new Vector3(a1.x + a2.x, a1.y + a2.y, a1.z + a2.z);
		return a3;
	}

	public Vector3 addX(int x)
	{
		this.x += x;
		return this;
	}
	public Vector3 TiletoPixel()
	{
		return new Vector3(x << 4, y << 4, z);
	}
	public Vector3 PixeltoTile()
	{
		return new Vector3(x >> 4, y >> 4, z);
	}

	public Vector3 addY(int y)
	{
//		System.out.println(y);
		this.y += y;
		return this;
	}

	public Vector3 addZ(int z)
	{
//		System.out.println(x);
		this.z += z;
		return this;
	}
	public boolean IsNull() 
	{
		return isNull;
	}

	public Vector3 subVectors(Vector3 a1, Vector3 a2)
	{
		Vector3 a3 = new Vector3(a1.x - a2.x, a1.y - a2.y, a1.z - a2.z);
		return a3;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public int getZ()
	{
		return z;
	}

	public int getXY(int width)
	{
		return x + y * width;
	}

	public String getXYTest()
	{
		return "x:" + x + " y:" + y;
	}

	public String toString()
	{
		return "x:" + x + " y:" + y + " z:" + z;
	}

	@Override
	public boolean equals(Object vector3)
	{
//		System.out.println(((Vector3) vector3).getXYZTest());
//		System.out.println(((Vector3) this).getXYZTest());
		if (((Vector3) vector3).getX() == getX() && ((Vector3) vector3).getY() == getY())
		{
			return true;
		}
		return false;
	}
}
