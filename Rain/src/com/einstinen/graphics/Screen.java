package com.einstinen.graphics;

import com.einstinen.Game;
import com.einstinen.level.tile.Tile;

public class Screen
{
	/**
	 * the average sprite size.
	 */
	private final int SPRITESIZE = Sprite.AverageSprite; 
	/**
	 * the width and the height of the screen.
	 */
	public int width, height;
	/**
	 * the array of all the pixels on the screen.
	 */
	public int[] pixels;
	/**
	 * the array that holds the spesific colors the backgound will be for the for at the same index.
	 */
	private int[] floorCol;
//	private int segment;

	/**
	 * the offsets for rendering.
	 */
	public int xOffset, yOffset;

	// private Random random = new Random();

	public Screen(int width, int height)
	{
		this.width = width;
		this.height = height;
		pixels = new int[width * height];
		floorCol = new int[Game.gameDepth];
//		segment = floorCol.length / 4;
		setCol();
	}

	// fix clear method to save memory
	public void clear()
	{
		for (int i = 0; i < pixels.length; i++)
		{
			pixels[i] = 0;
		}
	}

	private void setCol()
	{
		// segment 3
		floorCol[31] = 0xff00c900;
		floorCol[30] = 0xff00c900;
		floorCol[29] = 0xff00c900;
		// segment 2
		floorCol[28] = 0xffb4b40e;
		floorCol[27] = 0xffb4b40e;
		floorCol[26] = 0xffb4b40e;
		floorCol[25] = 0xffb4b40e;

		floorCol[24] = 0xffA47226;
		floorCol[23] = 0xffA47226;
		floorCol[22] = 0xffA47226;
		floorCol[21] = 0xffA47226;

		// segment 1
		floorCol[20] = 0xff523994;
		floorCol[19] = 0xff523994;
		floorCol[18] = 0xff523994;
		floorCol[17] = 0xff523994;

		floorCol[16] = 0xff0000ff;
		floorCol[15] = 0xff0000ff;
		floorCol[14] = 0xff0000ff;
		floorCol[13] = 0xff0000ff;

		floorCol[12] = 0xff4100be;
		floorCol[11] = 0xff4100be;
		floorCol[10] = 0xff4100be;
		floorCol[9] = 0xff4100be;

		// segment 0
		floorCol[8] = 0xff7f007f;
		floorCol[7] = 0xff7f007f;
		floorCol[6] = 0xff7f007f;
		floorCol[5] = 0xff7f007f;

		floorCol[4] = 0xffBE0041;
		floorCol[3] = 0xffBE0041;
		floorCol[2] = 0xffBE0041;

		floorCol[1] = 0xffff0000;
		floorCol[0] = 0xffff0000;
	}

	public void renderTile(int xp, int yp, int z, Tile tile)
	{
		if (z == Game.worldLevel)
		{
			xp -= xOffset;
			yp -= yOffset;
			for (int y = 0; y < tile.sprite.SIZE; y++)
			{
				int ya = y + yp;
				for (int x = 0; x < tile.sprite.SIZE; x++)
				{
					int xa = x + xp;
					if (xa < 0)
						xa = 0;
					if (ya < 0)
						ya = 0;
					if (xa >= width || ya >= height)
						break;
					int col = 0xff00c900;
					if (tile.sprite.pixels[x + y * tile.sprite.SIZE] == col)
						pixels[xa + ya * width] = floorCol[Game.worldLevel];

					else
						pixels[xa + ya * width] = tile.sprite.pixels[x + y * tile.sprite.SIZE];
				}
			}
		}
	}

	public void renderMob(int xp, int yp, int z, boolean flippedX, boolean flippedY, Sprite sprite)
	{
		if (inRange(xp, yp))
		{
			if (z == Game.worldLevel)
			{
				xp -= xOffset;
				yp -= yOffset;
				for (int y = 0; y < sprite.SIZE; y++)
				{
					int ya = y + yp;
					for (int x = 0; x < sprite.SIZE; x++)
					{
						int xa = x + xp;
						if (xa < 0)
							xa = 0;
						if (ya < 0)
							ya = 0;
						if (xa >= width || ya >= height)
							break;
						if (flippedY)
						{
							if (flippedX)
							{
								if (sprite.pixels[15 - x + (y - 15) * sprite.SIZE] != 0xffffff00)
								{
									pixels[xa + ya * width] = sprite.pixels[x + y * sprite.SIZE];
								}
							} else
							{
								if (sprite.pixels[x + (y - 15) * sprite.SIZE] != 0xffffff00)
									pixels[xa + ya * width] = sprite.pixels[x + y * sprite.SIZE];
							}
						} else
						{
							if (flippedX)
							{
								if (sprite.pixels[15 - x + y * sprite.SIZE] != 0xffffff00)
								{
									pixels[xa + ya * width] = sprite.pixels[x + y * sprite.SIZE];
								}
							} else
							{
								if (sprite.pixels[x + y * sprite.SIZE] != 0xffffff00)
									pixels[xa + ya * width] = sprite.pixels[x + y * sprite.SIZE];
							}
						}
					}
				}
			}
		}
	}

	public void setOffset(int xOffset, int yOffset)
	{
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}

	private boolean inRange(int xp, int yp)
	{
		if (xp >= Game.getPlayer().getX() - this.width / 2 - 16 && xp < Game.getPlayer().getX() + this.width / 2)
		{
			if (yp >= Game.getPlayer().getY() - this.height / 2 - 16 && yp < Game.getPlayer().getY() + this.height / 2)
			{
				return true;
			}
		}
		return false;
	}
}
