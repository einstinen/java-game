package com.einstinen.graphics;

public class Sprite
{
	public static final int AverageSprite = 16;
	public final int SIZE;
	private int x, y;
	public int[] pixels;
	private SpriteSheet sheet;

	public static Sprite grass = new Sprite(AverageSprite, 0, 0, SpriteSheet.tiles);
	public static Sprite grass1 = new Sprite(AverageSprite, 1, 0, SpriteSheet.tiles);
	public static Sprite grass2 = new Sprite(AverageSprite, 2, 0, SpriteSheet.tiles);
	
	public static Sprite chicken = new Sprite(AverageSprite, 3, 0, SpriteSheet.tiles);

	public static Sprite stone = new Sprite(AverageSprite, 0, 1, SpriteSheet.tiles);

	public static Sprite floor = new Sprite(AverageSprite, 1, 1, SpriteSheet.tiles);
	public static Sprite path = new Sprite(AverageSprite, 3, 1, SpriteSheet.tiles);

	public static Sprite oreFloor = new Sprite(AverageSprite, 2, 2, SpriteSheet.tiles);
	public static Sprite ore = new Sprite(AverageSprite, 2, 1, SpriteSheet.tiles);

	public static Sprite Cwall = new Sprite(AverageSprite, 1, 2, SpriteSheet.tiles);
	public static Sprite Hwall = new Sprite(AverageSprite, 0, 2, SpriteSheet.tiles);
	public static Sprite Vwall = new Sprite(AverageSprite, 0, 3, SpriteSheet.tiles);
	
	public static Sprite up = new Sprite(AverageSprite, 2, 3, SpriteSheet.tiles);
	public static Sprite down = new Sprite(AverageSprite, 1, 3, SpriteSheet.tiles);

	//readyPlayerOne
	public static Sprite player0 = new Sprite(AverageSprite, 0, 0, SpriteSheet.mobOne);
	public static Sprite player1 = new Sprite(AverageSprite, 1, 0, SpriteSheet.mobOne);
	
	public static Sprite nullSprite = new Sprite(AverageSprite, 15, 15, SpriteSheet.tiles);
	public Sprite(int size, int x, int y, SpriteSheet sheet)
	{
		// implement animation
		SIZE = size;
		pixels = new int[SIZE * SIZE];
		this.x = x * size;
		this.y = y * size;
		this.sheet = sheet;
		load();
	}

	private void load()
	{
		for (int y = 0; y < SIZE; y++)
		{
			for (int x = 0; x < SIZE; x++)
			{
				pixels[x + y * SIZE] = sheet.pixels[(x + this.x)
						+ (y + this.y) * sheet.SIZE];
			}
		}
	}
}
