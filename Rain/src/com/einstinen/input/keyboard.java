package com.einstinen.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class keyboard implements KeyListener
{
	//make the scroll lock do something
	private boolean[] keys= new boolean[146];
	private boolean[] keyType = new boolean[146];
	public boolean up, down, left, right, scroll, per, com;
	public void update()
	{
		up = keys[KeyEvent.VK_UP] || keys[KeyEvent.VK_W];
		down = keys[KeyEvent.VK_DOWN] || keys[KeyEvent.VK_S];
		left = keys[KeyEvent.VK_LEFT] || keys[KeyEvent.VK_A];
		right = keys[KeyEvent.VK_RIGHT] || keys[KeyEvent.VK_D];		
		per = keys[KeyEvent.VK_PERIOD];
		com = keys[KeyEvent.VK_COMMA];
		scroll = keys[KeyEvent.VK_SCROLL_LOCK];
	}
	public void keyPressed(KeyEvent e)
	{
		keys[e.getKeyCode()] = true;
	}

	public void keyReleased(KeyEvent e)
	{
		keys[e.getKeyCode()] = false;
		
	}

	public void keyTyped(KeyEvent e)
	{
		if(keyType[e.getKeyCode()])
			keyType[e.getKeyCode()] = false;
		else
			keyType[e.getKeyCode()] = true;
	}

}
