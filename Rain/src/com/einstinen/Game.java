package com.einstinen;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.swing.JFrame;

import com.einstinen.entity.mob.Mob;
import com.einstinen.entity.mob.Player;
import com.einstinen.graphics.Screen;
import com.einstinen.input.keyboard;
import com.einstinen.level.Level;
import com.einstinen.level.RandomLevel;

public class Game extends Canvas implements Runnable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int width = 300;
	public static int height = width / 16 * 9;
	public static int xCam = 0, yCam = 0;
	public static int scale = 3;
	public static int worldLevel;
	public static int gameWidth = 64, gameHeight = 64, gameDepth = 32;
	
	private Thread thread;
	private JFrame frame;
	private keyboard key;
	public static Level Currentlevel;
	//private static Level[] stored levels = new Level[num]
	private static Player player;
	private boolean running = false;

	private Screen screen;

	private BufferedImage image = new BufferedImage(width, height,
			BufferedImage.TYPE_INT_RGB);
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer())
			.getData();

	public Game()
	{
		Dimension size = new Dimension(width * scale, height * scale);
		setPreferredSize(size);

		screen = new Screen(width, height);
		frame = new JFrame();
		key = new keyboard();
		Currentlevel = new RandomLevel(gameWidth, gameHeight, gameDepth);
		player = new Player(key, Currentlevel, gameDepth - 1);
		addKeyListener(key);
	}

	public synchronized void start()
	{
		running = true;
		thread = new Thread(this, "Display");
		thread.start();
	}

	public synchronized void stop()
	{
		running = false;
		try
		{
			thread.join();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void run()
	{
		long lastTime = System.nanoTime();
		long timer = System.currentTimeMillis();
		final double ns = 1000000000.0 / 60.0;
		double delta = 0;
		int updates = 0;
		requestFocus();
		while (running)
		{
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1)
			{
				update();
				render();
				updates++;
				delta--;
			}
			if (System.currentTimeMillis() - timer > 1000)
			{
				frame.setTitle("Rain (" + (player.getX() >> 4) + ", " + (player.getY() >> 4) + ") : " + updates);
				timer = System.currentTimeMillis();
				updates = 0;
			}
		}
		stop();
	}


	public void update()
	{
		key.update();
		worldLevel = player.getZ();
		for (Mob mob : Mob.Mobs)
		{
			if (mob.getZ() == player.getZ() || mob.getZ() == player.getZ() - 1 || mob.getZ() == player.getZ() + 1)
			mob.update();
		}
		for(int y = 0; y < gameHeight; y++)
		{
			for(int x = 0; x < gameWidth; x++)
			{
				Currentlevel.getTile(x, y, player.getZ()).update();
			}
		}
	}

	public void render()
	{
		BufferStrategy bs = getBufferStrategy();
		if (bs == null)
		{
			createBufferStrategy(3);
			return;
		}
		screen.clear();
		if(player.getX() > screen.width / 2 && player.getX() < gameWidth * 16 - screen.width / 2)
			xCam = player.getX() - screen.width / 2;
		if(player.getY() > screen.height / 2 && player.getY() < gameHeight * 16 - screen.height / 2)
			yCam = player.getY() - screen.height / 2;
		Currentlevel.render(xCam, yCam, screen);
		for (Mob mob : Mob.Mobs)
		{
			mob.render(screen);
		}
		for (int i = 0; i < pixels.length; i++)
		{
			pixels[i] = screen.pixels[i];
		}
		Graphics g = bs.getDrawGraphics();
		// graphics{
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		// }graphics
		g.dispose();
		bs.show();
	}

	public static void main(String[] args)
	{
		Game game = new Game();
		game.frame.setResizable(false);
		game.frame.setTitle("Rain");
		game.frame.add(game);
		game.frame.pack();
		game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		game.frame.setLocationRelativeTo(null);
		game.frame.setVisible(true);
		
		game.start();
	}
	public static Player getPlayer()
	{
		return player;
	}
}
